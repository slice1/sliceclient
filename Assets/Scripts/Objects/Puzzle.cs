﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Puzzle : MonoBehaviour
{

    public GameObject mainMenu, WarningPanel;
    public Text WarningText;
    public Texture2D SandImage, C1IMG, C2IMG, C3IMG;
    public int blocksPerLine = 4;
    public Plane mainPlane;
    public int difficulty = 0;
    public float durration = 4;
    public int countOfPlayer = 2;
    public Text level;
    public Shader MYshader;


    public GameBuff gameBuff = new GameBuff();
    public VisualSystem visualSystem;
    private AlertSystem alertSystem;
    public GameLogic gameLogic;
    public Lobby lobby;

    public PersonaDescrPanelScript personaDescrScr;


    public static Dictionary<int, Faction> Factions = new Dictionary<int, Faction>();


    public LoyVisit loyVisit = new LoyVisit();


    public GameObject tilePrefab;
    public GameObject FonQuad;

    public bool CanPlayerMove = true;

    public Queue<List<Block>> QueueOfQueue = new Queue<List<Block>>();
    public int BlockIsMove = 0;
    public CharacterGenerator charGen = new CharacterGenerator();

    public Block chosenBlock;
    public bool doAct = false;
    public bool doInterrogation = false;

    public List<List<Block>> BlockMap = new List<List<Block>>();
    public int test = 0;

    Texture2D[] imgs = new Texture2D[4];

    void Start()
    {

        Faction Police = new Faction(1, "Police");
        Faction Mafia = new Faction(2, "Mafia");
        Faction Cult = new Faction(3, "Cult");
        Faction Assasins = new Faction(4, "Assasins");

        Factions.Add(1, Police);
        Factions.Add(2, Mafia);
        Factions.Add(3, Cult);
        Factions.Add(4, Assasins);


        alertSystem = new AlertSystem(visualSystem);    
        charGen.GenInit(alertSystem);
    }


    public Faction GetFactionByID(int ID)
    {
        return Factions[ID];
    }

    public void chooseBlock(Block block)
    {
        if (doAct)
        {
            chosenBlock.doAct(block);
        }
        else if (doInterrogation)
        {
            chosenBlock.doInterrogation(block);
        }
        else
        {
            chosenBlock = block;
            personaDescrScr._char = chosenBlock._character;
            personaDescrScr.syncData();
        }


    }




    public void ActVisit()
    {
        foreach (List<Block> list in BlockMap)
        {
            foreach (Block b in list)
            {
                b._character.AcceptVisit(loyVisit);
            }
        }
    }






    public void actBtnClk()
    {
        if (chosenBlock != null)
        {
            doAct = true;
            doInterrogation = false;
        }
    }

    public void interrogatBtnClk()
    {
        if (chosenBlock != null)
        {
            doAct = false;
            doInterrogation = true;
        }
    }




    #region memento

    public void SaveGame()
    {
        gameBuff.History.Push(saveState(chosenBlock));
    }


    public void RestoreGame()
    {
        restoreState(gameBuff.History.Pop());
    }

    Memento saveState(Block chosen)
    {
        return new Memento(chosen._character);
    }

    void restoreState(Memento saved)
    {
        chosenBlock._character._firstName = saved.name;
        chosenBlock._character._power = saved.pow;
        chosenBlock._character._loyalty = saved.loy;
        chosenBlock._character._wisdom = saved.wis;
        chosenBlock._character._sprite = saved.sprite;
    }

    #endregion memento



    public void BlockMoved()
    {



    }



    void shufle()
    {
        for (int i = 0; i < difficulty; i++)
        {
            int x;
            int y;
            if (Random.value > 0.5f)
            {
                y = Random.Range(0, blocksPerLine);
                BlockMap[Random.Range(0, blocksPerLine)][y].MoveBlockToCoorAtMap(Random.Range(0, blocksPerLine), y, Random.Range(0, 2) * 2 + 1);
            }
            else
            {
                x = Random.Range(0, blocksPerLine);
                BlockMap[x][Random.Range(0, blocksPerLine)].MoveBlockToCoorAtMap(x, Random.Range(0, blocksPerLine), Random.Range(0, 2) * 2 + 2);
            }
        }
    }

    public void Warning(string str)
    {
        alertSystem.Warn(str);
    }

    void OnBlockFinishMove()
    {
        BlockIsMove--;

        CheckNeedOfAnime();
    }



    public void CheckNeedOfAnime()
    {
        if (BlockIsMove == 0)
        {
            Warning("");
            CanPlayerMove = true;
        }
        while (QueueOfQueue.Count > 0 && BlockIsMove < blocksPerLine)
        {
            
            StartCoroutine(StartMoveList(QueueOfQueue.Dequeue()));
            //foreach (Block b in QueueOfQueue.Dequeue())
            //{
            //    //Vector2[] vec = b.coordsToMoveFromBlockData();
            //    b.NEXTAnimateMove();
            //}
        }

    }


    IEnumerator StartMoveList(List<Block> arr)
    {
        test++;
        foreach (Block b in arr)
        {
            b.NEXTAnimateMove();
        }

        if (BlockIsMove < blocksPerLine)
            yield return null;
    }


    #region CreatePuzzle

    //void CreatePuzzle()
    //{
    //    mainPlane = new Plane(Vector3.left, Vector3.up, Vector3.zero);

    //    //Debug.Log("Creating map");
    //    for (int i = 0; i < blocksPerLine; i++)
    //    {
    //        List<Block> list = new List<Block>();
    //        for (int j = 0; j < blocksPerLine; j++)
    //        {
    //            list.Add(new Block());
    //        }
    //        BlockMap.Add(list);
    //    }

    //    float otstup = blocksPerLine / 2 + .5f * (blocksPerLine % 2);
        
    //    for (int y = 0; y < blocksPerLine; y++)
    //    {
    //        for (int x = 0; x < blocksPerLine; x++)
    //        {

    //            GameObject blockObject = Instantiate(tilePrefab);
    //            blockObject.transform.position = -Vector2.one * (blocksPerLine - 1) * .5f + new Vector2(x + otstup, y + otstup);
    //            blockObject.transform.parent = transform;



    //            Block block = blockObject.GetComponentInChildren<Block>();
    //            block.OnFinishMoving += OnBlockFinishMove;
    //            // block.OnBlockPressed += PlayerMoveBlockInput;


    //            block.Init(charGen.CreateCharacter(), mainPlane, this, x, y, block.GetComponent<Renderer>().bounds.size.x, BlockMap, MYshader);


    //            //if (y == 0 && x == blocksPerLine - 1)
    //            //{
    //            //    blockObject.SetActive(false);
    //            //    emptyBlock = block;
    //            //}
    //        }
    //    }



    //    Camera.main.transform.position = Camera.main.transform.position + new Vector3(Camera.main.transform.position.x + otstup, Camera.main.transform.position.y + otstup);
    //    Camera.main.orthographicSize = blocksPerLine * .55f;
        

    //    //shufle();
    //}


    public void CreatePuzzle(List<List<Character>> characters)
    {
        mainPlane = new Plane(Vector3.left, Vector3.up, Vector3.zero);

        blocksPerLine = characters.Count;

        for (int i = 0; i < characters.Count; i++)
        {
            List<Block> list = new List<Block>();
            for (int j = 0; j < characters[i].Count; j++)
            {
                list.Add(new Block());
            }
            BlockMap.Add(list);
        }

        float otstup = blocksPerLine / 2 + .5f * (blocksPerLine % 2);



        for (int y = 0; y < characters.Count; y++)
        {
            for (int x = 0; x < characters[y].Count; x++)
            {

                GameObject blockObject = Instantiate(tilePrefab);
                blockObject.transform.position = -Vector2.one * (blocksPerLine - 1) * .5f + new Vector2(x + otstup, y + otstup);
                blockObject.transform.parent = transform;



                Block block = blockObject.GetComponentInChildren<Block>();
                block.OnFinishMoving += OnBlockFinishMove;


                block.Init(characters[y][x], mainPlane, this, x, y, block.GetComponent<Renderer>().bounds.size.x, BlockMap, MYshader);



            }
        }



        Camera.main.transform.position = Camera.main.transform.position + new Vector3(Camera.main.transform.position.x + otstup, Camera.main.transform.position.y + otstup);
        Camera.main.orthographicSize = blocksPerLine * .55f;

        FonQuad.transform.position = Camera.main.transform.position + new Vector3(0, 0, 40);
        FonQuad.transform.localScale += new Vector3(24f, 24f);
    }


    #endregion CreatePuzzle

    public void Exit()
    {
        Application.Quit();
    }

    public void BackToMenu()
    {
        StopAllCoroutines();
        QueueOfQueue.Clear();
        MapWipe();
        Warning("");
        mainMenu.SetActive(true);
    }

    public void MapWipe()
    {
        Debug.Log("Map Wipe process started");
        try
        {
            for (int x = 0; x < blocksPerLine; x++)
            {
                for (int y = 0; y < blocksPerLine; y++)
                {
                    BlockMap[x][y].WipeInfo();
                    Destroy(BlockMap[x][y].gameObject);
                }
            }
        }
        catch
        {

        }
        BlockMap.Clear();
        Camera.main.transform.position = new Vector3(0,0,-10);
    }


    public void checkVictory()
    {
        //int check = 0;
        //foreach (List<Block> list in BlockMap)
        //{
        //    foreach (Block b in list)
        //    {
        //        if (!b.checkStartPos())
        //            break;
        //        check++;
        //    }
        //}

        //if (check == blocksPerLine * blocksPerLine)
        //    Victory();
    }



    void Victory()
    {
        MapWipe();
    }
}
