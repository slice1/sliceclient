﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Faction _faction;
    public string _name;
    public int _money;
    public int _index;
}
