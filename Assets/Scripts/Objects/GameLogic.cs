﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLogic : MonoBehaviour
{
    [Header("Network Links")]
    public ClientSendData clientSendData;

    [Header("Game Links")]
    public Puzzle puzzle;
    public Lobby lobby;

    [Header("Player Data")]
    public int _countOfPlayers, _curPlayer = 0;
    public List<Player> players = new List<Player>();

    [Header("Turn Data")]
    public bool isMoved = false;
    public bool isActed = false;
    public bool isItMyTurn = false;

    private void SyncWithLobbyPlayerList(List<Player> list)
    {
        list.Clear();

        if (lobby.player1 == null)
        {
            players.Add(lobby.player1);
            _countOfPlayers++;
        }

        if (lobby.player2 == null)
        {
            players.Add(lobby.player2);
            _countOfPlayers++;
        }

        if (lobby.player3 == null)
        {
            players.Add(lobby.player3);
            _countOfPlayers++;
        }
        if (lobby.player4 == null)
        {
            players.Add(lobby.player4);
            _countOfPlayers++;
        }
    }

    public void StartGame()
    {
        SyncWithLobbyPlayerList(players);


    }
    public void MoveBlock(int X, int Y, int dir, int lastX, int lastY)
    {
        if (X == lastX && Y == lastY)
        {
            return;
        }
        isMoved = true;
        int distan = System.Convert.ToInt32(Mathf.Sqrt(Mathf.Pow((X - lastX), 2) + Mathf.Pow((Y - lastY), 2)));

        clientSendData.SendMoveBlock(lastX, lastY, dir, distan);
    }

    public void EndTurn()
    {
        isItMyTurn = false;
        NextPlayer();
    }

    public void StartTurn()
    {
        isMoved = false;
        isActed = false;
        isItMyTurn = true;
    }

    public void NextPlayer()
    {
        _curPlayer++;

        if (_curPlayer == _countOfPlayers)
            _curPlayer = 0;
    }
}
