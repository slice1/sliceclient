﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lobby : MonoBehaviour
{
    public Player player1, player2, player3, player4;
    public Puzzle game;
    public Player OwnPlayer;

    public Text tp1, tp2, tp3, tp4;
    public int _ServerIndex;

    public void Update()
    {
        if (player1 != null)
            tp1.text = player1._name;
        else
            tp1.text = "";

        if (player2 != null)
            tp2.text = player2._name;
        else
            tp2.text = "";

        if (player3 != null)
            tp3.text = player3._name;
        else
            tp3.text = "";

        if (player4 != null)
            tp4.text = player4._name;
        else
            tp4.text = "";

    }

    public void SetOwnPlayer(string name)
    {
        if (player1._name == name)
        {
            OwnPlayer = player1;
        }
        else if (player2._name == name)
        {
            OwnPlayer = player2;
        }
        else if (player3._name == name)
        {
            OwnPlayer = player3;
        }
        else if (player4._name == name)
        {
            OwnPlayer = player4;
        }
        else
        {
            Debug.Log("Invalid name of player!");
        }
    }

    public void AddPlayer(string name, int index)
    {
        if (player1 == null)
        {
            player1 = gameObject.AddComponent<Player>();
            player1._name = name;
            player1._index = index;
        }
        else if (player2 == null)
        {
            player2 = gameObject.AddComponent<Player>();
            player2._name = name;
            player2._index = index;

        }
        else if (player3 == null)
        {
            player3 = gameObject.AddComponent<Player>();
            player3._name = name;
            player3._index = index;

        }
        else if (player4 == null)
        {
            player4 = gameObject.AddComponent<Player>();
            player4._name = name;
            player4._index = index;

        }
        else
        {
            Debug.Log("Invalid count of players!");
        }

    }

    public void SetFactionByName(string str, int id)
    {
        if (player1._name == str)
        {
            player1._faction = game.GetFactionByID(id);
        }
        else if (player2._name == str)
        {
            player2._faction = game.GetFactionByID(id);
        }
        else if (player3._name == str)
        {
            player3._faction = game.GetFactionByID(id);
        }
        else if (player4._name == str)
        {
            player4._faction = game.GetFactionByID(id);
        }
        else
        {
            Debug.Log("Invalid name of player!");
        }
    }

    public void KickPlayer(string name)
    {


    }

}
