﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Faction 
{
    public int _ID;
    public string _name;

    public Faction(int ID, string name)
    {
        _ID = ID;
        _name = name;
    }
}

