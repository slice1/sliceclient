﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileCharacterScr : MonoBehaviour
{ 

    public Character _char;
    public SpriteRenderer renderer;
    public TextMesh textMesh;


    void Update()
    {
        renderer.sprite = _char._sprite;
        textMesh.text = _char._firstName + " " + _char._lastName;
    }
}
