﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RoleScr : ScriptableObject, IDescrBox
{
    public abstract void Act(Character character);
    public abstract void ChangeRole(Character character, RoleScr roleScr);

    public abstract string GetDescription();
    public abstract string GetName();
}

public class SpyRole : RoleScr
{
    public override void Act(Character character)
    {
        throw new System.NotImplementedException();
    }

    public override void ChangeRole(Character character, RoleScr roleScr)
    {
        throw new System.NotImplementedException();
    }

    public override string GetDescription()
    {
        throw new System.NotImplementedException();
    }

    public override string GetName()
    {
        throw new System.NotImplementedException();
    }
}

public class PropagandeRole : RoleScr
{
    public override void Act(Character character)
    {
        throw new System.NotImplementedException();
    }

    public override void ChangeRole(Character character, RoleScr roleScr)
    {
        throw new System.NotImplementedException();
    }

    public override string GetDescription()
    {
        throw new System.NotImplementedException();
    }

    public override string GetName()
    {
        throw new System.NotImplementedException();
    }
}

public class ClassicRole : RoleScr
{
    public override void Act(Character character)
    {
        throw new System.NotImplementedException();
    }

    public override void ChangeRole(Character character, RoleScr roleScr)
    {
        throw new System.NotImplementedException();
    }

    public override string GetDescription()
    {
        throw new System.NotImplementedException();
    }

    public override string GetName()
    {
        throw new System.NotImplementedException();
    }
}

public class T2Role : RoleScr
{
    public override void Act(Character character)
    {
        throw new System.NotImplementedException();
    }

    public override void ChangeRole(Character character, RoleScr roleScr)
    {
        throw new System.NotImplementedException();
    }

    public override string GetDescription()
    {
        throw new System.NotImplementedException();
    }

    public override string GetName()
    {
        throw new System.NotImplementedException();
    }
}