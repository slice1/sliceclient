﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public abstract class TrustState
{
    public Character _character;

    public TrustState(Character character)
    {
        _character = character;
    }

    public abstract void UpTrust(Character character, Faction faction);
    public abstract void DownTrust(Character character, Faction faction);


    public abstract int RetLoyalty(Character character);

    public abstract void Interogation(Character character);

    public abstract void UseItem(Character character);

    public abstract void DoAction(Character character);
}

class VeryLowTrustState : TrustState
{
    public VeryLowTrustState(Character characte)
    : base(characte)
    {
        _character = characte;
    }


    public override void UpTrust(Character character, Faction faction)
    {
        character.SetTrustToFaction(faction, new LowTrustState(_character));
    }

    public override void DownTrust(Character character, Faction faction)
    {

    }

    public override void DoAction(Character character)
    {

    }

    public override void Interogation(Character character)
    {

    }

    public override int RetLoyalty(Character character)
    {
        return character._loyalty / 2;
    }



    public override void UseItem(Character character)
    {

    }
}

class LowTrustState : TrustState
{
    public LowTrustState(Character characte)
    : base(characte)
    {
        _character = characte;
    }

    public override void UpTrust(Character character, Faction faction)
    {
        character.SetTrustToFaction(faction, new MiddleTrustState(_character));
    }



    public override void DownTrust(Character character, Faction faction)
    {
        character.SetTrustToFaction(faction, new VeryLowTrustState(_character));
    }

    public override void DoAction(Character character)
    {

    }

    public override void Interogation(Character character)
    {

    }

    public override int RetLoyalty(Character character)
    {
        return character._loyalty - 10;
    }



    public override void UseItem(Character character)
    {

    }
}

class MiddleTrustState : TrustState
{
    public MiddleTrustState(Character characte)
    : base(characte)
    {
        _character = characte;
    }


    public override void UpTrust(Character character, Faction faction)
    {
        character.SetTrustToFaction(faction, new HighTrustState(_character));
    }

    public override void DownTrust(Character character, Faction faction)
    {
        character.SetTrustToFaction(faction, new LowTrustState(_character));
    }

    public override void DoAction(Character character)
    {

    }

    public override void Interogation(Character character)
    {

    }

    public override int RetLoyalty(Character character)
    {
        return character._loyalty;
    }



    public override void UseItem(Character character)
    {

    }
}

class HighTrustState : TrustState
{
    public HighTrustState(Character characte)
: base(characte)
    {
        _character = characte;
    }

    public override void UpTrust(Character character, Faction faction)
    {
        character.SetTrustToFaction(faction, new VeryHighTrustState(_character));
    }

    public override void DownTrust(Character character, Faction faction)
    {
        character.SetTrustToFaction(faction, new MiddleTrustState(_character));
    }

    public override void DoAction(Character character)
    {

    }

    public override void Interogation(Character character)
    {

    }

    public override int RetLoyalty(Character character)
    {
        return character._loyalty/2+character._loyalty;
    }

    public override void UseItem(Character character)
    {

    }
}

class VeryHighTrustState : TrustState
{
    public VeryHighTrustState(Character characte)
: base(characte)
    {
        _character = characte;
    }

    public override void UpTrust(Character character, Faction faction)
    {
        Debug.Log("Maxim Trust " + faction._name);
    }

    public override void DownTrust(Character character, Faction faction)
    {
        character.SetTrustToFaction(faction, new HighTrustState(_character));
    }

    public override void DoAction(Character character)
    {

    }

    public override void Interogation(Character character)
    {

    }

    public override int RetLoyalty(Character character)
    {
        return character._loyalty*2;
    }



    public override void UseItem(Character character)
    {

    }
}

class UltimaTrustState : TrustState
{
    public UltimaTrustState(Character characte)
: base(characte)
    {
        _character = characte;
    }

    public override void UpTrust(Character character, Faction faction)
    {

    }



    public override void DownTrust(Character character, Faction faction)
    {

    }

    public override void DoAction(Character character)
    {

    }

    public override void Interogation(Character character)
    {

    }

    public override int RetLoyalty(Character character)
    {
        return character._loyalty*3;
    }



    public override void UseItem(Character character)
    {

    }
}









public class Character : ScriptableObject
{
    public string _firstName, _lastName;
    public Sprite _sprite;

    public bool _male;


    public int _power, _wisdom, _loyalty;

    public InvItem _invItem;

    AlertSystem alert;
    private Dictionary<int, TrustState> TrustDict = new Dictionary<int, TrustState>();

    public Character(string firstName, string lastName, bool IsMale, int power, int wisdom, int loyalty, Sprite sprite, AlertSystem alertSystem, int PoliceLoyal, int MafiaLoyal, int AssasinLoyal, int CultLoyal)
    {
        alert = alertSystem;

        _firstName = firstName;
        _lastName = lastName;
        _male = IsMale;
        _sprite = sprite;


        _power = power;
        _wisdom = wisdom;
        _loyalty = loyalty;
        trustInit(GENTrsustByInt(PoliceLoyal, this), GENTrsustByInt(MafiaLoyal, this), GENTrsustByInt(AssasinLoyal, this), GENTrsustByInt(CultLoyal, this));
    }

    private TrustState GENTrsustByInt(int i, Character c)
    {
        switch (i)
        {
            case 0:
                return new VeryLowTrustState(c);
            case 1:
                return new LowTrustState(c);
            case 2:
                return new MiddleTrustState(c);
            case 3:
                return new HighTrustState(c);
            case 4:
                return new VeryHighTrustState(c);
            case 5:
                return new UltimaTrustState(c);
            default:
                Debug.Log("Unexpected TrustState by INT via GEN");
                return null;
        }     
    }

    private void trustInit(TrustState trustPolice, TrustState trustMafia, TrustState trustAssasin, TrustState trustCult)
    {
        TrustDict.Add(1, trustPolice);
        TrustDict.Add(2, trustMafia);
        TrustDict.Add(3, trustAssasin);
        TrustDict.Add(4, trustCult);
    }

    public TrustState TrustByFaction(Faction faction)
    {
        return TrustDict[faction._ID];
    }

    public void SetTrustToFaction(Faction faction, TrustState trustState)
    {
        TrustDict[faction._ID] = trustState;
    }

    public void Interrogation()
    {

    }

    public void UseItem()
    {

    }

    public void DoAct()
    {

    }

    public void AcceptVisit(Visitor visitor)
    {
        visitor.MakePower(this);
    }
}



public abstract class Visitor
{
    public abstract void MakePower(Character character);
}

public class LoyVisit : Visitor
{
    public override void MakePower(Character character)
    {
        if (character._loyalty > 20)
        {
            character._loyalty += 5;
        }
    }
}

public class WisVisit : Visitor
{
    public override void MakePower(Character character)
    {
        if (character._wisdom > 20)
        {
            character._wisdom += 5;
        }
    }
}
