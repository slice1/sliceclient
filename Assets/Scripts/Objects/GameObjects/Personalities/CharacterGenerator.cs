﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

using System.Linq;
using System.IO;

public class CharacterGenerator
{
    AlertSystem alert;
    List<Sprite> _malePortraits = new List<Sprite>();
    List<Sprite> _femPortraits = new List<Sprite>();


    MalTextHandler _malNamesHandler = new MalTextHandler();
    FemTextHandler _femNamesHandler = new FemTextHandler();

    List<int> malPortNumb = new List<int>();
    List<int> femPortNumb = new List<int>();

    public void GenInit(AlertSystem alertSystem)
    {
        alert = alertSystem;
        string tmp;

        List<Object> mobjs = new List<Object>();

        

        string path = "Portraits/malFaces";
        DirectoryInfo dataDir = new DirectoryInfo("Assets/Resources/" + path);
        try
        {
            FileInfo[] fileinfo = dataDir.GetFiles("*.jpg");
            for (int i = 0; i < fileinfo.Length; i++)
            {
                tmp = path + "/" + fileinfo[i].Name;
                tmp = tmp.Substring(0, tmp.Length-4);
                mobjs.Add(Resources.Load(tmp));
                
            }
        }
        catch (System.Exception e)
        {
            Debug.Log(e);
        }


        foreach (Object obj in mobjs)
        {

            System.Type type = obj.GetType();

            if (type == typeof(UnityEngine.Texture2D))
            {


                Texture2D tex = obj as Texture2D;

                Sprite newSprite = Sprite.Create(obj as Texture2D, new Rect(0f, 0f, tex.width, tex.height), Vector2.zero, 300);
                _malePortraits.Add(newSprite);
            }
        }







        List<Object> fobjs = new List<Object>();

        string Fpath = "Portraits/femFaces";
        DirectoryInfo FdataDir = new DirectoryInfo("Assets/Resources/" + Fpath);
        try
        {
            FileInfo[] fileinfo = FdataDir.GetFiles("*.jpg");
            for (int i = 0; i < fileinfo.Length; i++)
            {
                tmp = Fpath + "/" + fileinfo[i].Name;
                tmp = tmp.Substring(0, tmp.Length - 4);
                fobjs.Add(Resources.Load(tmp));

            }
        }
        catch (System.Exception e)
        {
            Debug.Log(e);
        }

        foreach (Object obj in fobjs)
        {

            System.Type type = obj.GetType();

            if (type == typeof(UnityEngine.Texture2D))
            {


                Texture2D tex = obj as Texture2D;

                Sprite newSprite = Sprite.Create(obj as Texture2D, new Rect(0f, 0f, tex.width, tex.height), Vector2.zero, 300);

                _femPortraits.Add(newSprite);
            }
        }
    }


    private int getMalPortNumb(int x)
    {
        if (malPortNumb.Count != _malePortraits.Count)
        {
            x = Random.Range(0, _malePortraits.Count);

            foreach (int i in malPortNumb)
            {
                if (x == i)
                {
                    getMalPortNumb(x);
                    break;
                }
            }

            malPortNumb.Add(x);
            return x;
        }
        else
        {
            malPortNumb.Clear();
            getMalPortNumb(x);
            return x;
        }
    }

    private int getFemPortNumb(int x)
    {
        if (femPortNumb.Count != _femPortraits.Count)
        {
            x = Random.Range(0, _femPortraits.Count);

            foreach (int i in femPortNumb)
            {
                if (x == i)
                {
                    getMalPortNumb(x);
                    break;
                }
            }

            femPortNumb.Add(x);
            return x;
        }
        else
        {
            femPortNumb.Clear();
            getFemPortNumb(x);
            return x;
        }
    }

    public Character CreateCharacter(int FirstName, int SecondName, int isMale, int power, int wisdom, int loyalty, int sprite, int INTPoliceLoyal, int INTMafiaLoyal, int INTAssasinLoyal, int INTCultLoyal)
    {
        Sprite _sprite;
        string _firstName;
        bool M;

        if (isMale == 1)
        {
            M = true;
            _sprite = _malePortraits[sprite];
            _firstName = _malNamesHandler.FileLines[FirstName];
        }
        else
        {
            M = false;
            _sprite = _femPortraits[sprite];
            _firstName = _femNamesHandler.FileLines[FirstName];
        }

        Character _character = new Character(_firstName, "", M, power, wisdom, loyalty, _sprite, alert, INTPoliceLoyal, INTMafiaLoyal, INTAssasinLoyal, INTCultLoyal);
        return _character;
    }



    public Character CreateCharacter()
    {

        int _power = Random.Range(15, 30);
        int _wisdom = Random.Range(15, 30);
        int _loyalty = Random.Range(15, 30);
        bool _ismale;

        Sprite _sprite;
        string _firstName;

        if (100 - Random.Range(0,100) > 35)
        {
            int r = 0;
            r = getMalPortNumb(r);
            _ismale = true;

            _sprite = _malePortraits[r];
            _firstName = _malNamesHandler.FileLines[Random.Range(0, _malNamesHandler.FileLines.Count)];
        }
        else
        {
            int r = 0;
            r = getFemPortNumb(r);
            _ismale = false;

            _sprite = _femPortraits[r];
            _firstName = _femNamesHandler.FileLines[Random.Range(0, _femNamesHandler.FileLines.Count)];
        }



        Character _character = new Character(_firstName, "", _ismale, _power, _wisdom, _loyalty, _sprite, alert, 2, 1, 0, 0);

        return _character;
    }
}



public class MalTextHandler
{
    // Enter the path of the file here
    private string filePath = "Assets/Resources/Portraits/maleNames.txt";

    private List<string> _fileLines;
    public List<string> FileLines
    {
        get
        {
            if (_fileLines != null) return _fileLines;
            string text = File.ReadAllText(filePath);
            if (string.IsNullOrEmpty(text))
            {
                // Make empty list
                _fileLines = new List<string>();
            }
            else if (!string.IsNullOrEmpty(text) && text.Any(f => f.Equals('\n')))
            {
                // Read text and split on comma
                _fileLines = text.Split('\n').ToList();
            }
            else
            {
                // Read lines instead
                _fileLines = File.ReadAllLines(filePath).ToList();
            }
            return _fileLines;
        }
        set
        {
            _fileLines = value;
            File.AppendAllText(filePath, string.Join("\n", _fileLines.ToArray()));
        }
    }

    /// <summary>
    /// Retrieve all up to date lines
    /// </summary>
    /// <returns></returns>
    public List<string> GetAllLines()
    {
        return File.ReadAllText(filePath).Split('\n').ToList();
    }

    /// <summary>
    /// Remove existing line from file if it exists
    /// </summary>
    /// <param name="line"></param>
    public void RemoveLine(string line)
    {
        // Remove line
        FileLines.Remove(line);

        // Save file
        File.AppendAllText(filePath, string.Join("\n", _fileLines.ToArray()));
    }

    public void AddLine(string line)
    {
        // Add line
        FileLines.Add(line);

        // Save file
        File.AppendAllText(filePath, string.Join("\n", _fileLines.ToArray()));
    }
}

public class FemTextHandler
{
    // Enter the path of the file here
    private string filePath = "Assets/Resources/Portraits/FemNames.txt";

    private List<string> _fileLines;
    public List<string> FileLines
    {
        get
        {
            if (_fileLines != null) return _fileLines;
            string text = File.ReadAllText(filePath);
            if (string.IsNullOrEmpty(text))
            {
                // Make empty list
                _fileLines = new List<string>();
            }
            else if (!string.IsNullOrEmpty(text) && text.Any(f => f.Equals('\n')))
            {
                // Read text and split on comma
                _fileLines = text.Split('\n').ToList();
            }
            else
            {
                // Read lines instead
                _fileLines = File.ReadAllLines(filePath).ToList();
            }
            return _fileLines;
        }
        set
        {
            _fileLines = value;
            File.AppendAllText(filePath, string.Join("\n", _fileLines.ToArray()));
        }
    }

    /// <summary>
    /// Retrieve all up to date lines
    /// </summary>
    /// <returns></returns>
    public List<string> GetAllLines()
    {
        return File.ReadAllText(filePath).Split('\n').ToList();
    }

    /// <summary>
    /// Remove existing line from file if it exists
    /// </summary>
    /// <param name="line"></param>
    public void RemoveLine(string line)
    {
        // Remove line
        FileLines.Remove(line);

        // Save file
        File.AppendAllText(filePath, string.Join("\n", _fileLines.ToArray()));
    }

    public void AddLine(string line)
    {
        // Add line
        FileLines.Add(line);

        // Save file
        File.AppendAllText(filePath, string.Join("\n", _fileLines.ToArray()));
    }
}

