﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAbility
{
    void RetAbil();
}

public abstract class InvItem : ScriptableObject, IDescrBox
{
    public abstract string GetDescription();
    public abstract string GetName();
}
