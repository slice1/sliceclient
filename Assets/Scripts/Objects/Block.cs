﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    public event System.Action OnFinishMoving;

    public Character _character;



    public SpriteRenderer renderer;
    public GameObject textMeshOBJ;
    private TextMesh textMesh;






    private Vector3 point;
    public Vector3 closeCoord_Pos;
    private Shader _shader;
    private float x;
    private float y;
    private float z;
    private Puzzle _puzzle;
    private Plane _planeBlock;
    private List<List<Block>> _MAP;
    //private Block closeCoord;
    private int closeCoord_X, closeCoord_Y;
    List<float[]> PosToMoveList = new List<float[]>();

    public int _X, _Y, _lastX, _lastY;
    private Vector3 _startCoord;

    private int _XStart, _YStart;
    private float _size;
    public List<Block> BlockArr = new List<Block>();
    public List<Block> QueueOfInputs = new List<Block>();

    private bool _BlockArrFull = false;

    private int _dirrection = 0;










    public void doAct(Block block)
    {

    }


    public void doInterrogation(Block block)
    {

    }



    private void Start()
    {
        textMesh = textMeshOBJ.GetComponent<TextMesh>();
    }


    void Update()
    {
        renderer.sprite = _character._sprite;
        textMesh.text = _character._firstName + " " + _character._lastName;
    }









    public bool checkStartPos()
    {
        if (_X == _XStart && _Y == _YStart)
            return true;
        return false;
    }

    public void Init(Character @chara, Plane @plane, Puzzle @puzzle, int x, int y, float @size, List<List<Block>> @map, Shader @shader)
    {
        _character = @chara;
        _planeBlock = @plane;
        _puzzle = @puzzle;
        _X = x;
        _Y = y;
        _shader = @shader;
        _XStart = _X;
        _YStart = _Y;
        _lastX = _X;
        _lastY = _Y;
        _MAP = @map;
        _MAP[x][y] = this;
        
        _size = @size;
        _startCoord = gameObject.transform.position;

        closeCoord_Pos = gameObject.transform.position;
        closeCoord_X = _X;
        closeCoord_Y = _Y;
    }



    Vector3 ScreenPosToWorldPosByPlane(Vector2 screenPos, Plane plane)
    {
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(screenPos.x, screenPos.y, 1f));
        float distance;
        if (!plane.Raycast(ray, out distance))
            throw new UnityException("did not hit plane");
        return ray.GetPoint(distance);
    }



    private void OnMouseDown()
    {
        _puzzle.chooseBlock(this);
    }



    void OnMouseDrag()
    {
        MoveBlockOnPos(Input.mousePosition.x, Input.mousePosition.y);
    }


    public void NEXTAnimateMove()
    {
        AnimateMoveToPos(new Vector2(PosToMoveList[0][0], PosToMoveList[0][1]), new Vector2(PosToMoveList[0][2], PosToMoveList[0][3]), _puzzle.durration);
        PosToMoveList.RemoveAt(0);
    }


    public void AnimateMoveToPos(Vector2 target, Vector2 dir, float duration)
    {
        StartCoroutine(AnimateMove(target, dir, duration));
    }

    public void WipeInfo()
    {
        PosToMoveList.Clear();
        BlockArr.Clear();
        QueueOfInputs.Clear();
       
    }



    IEnumerator AnimateMove(Vector2 target, Vector2 dir, float duration)
    {
        Vector2 initialPos = transform.position;

        _puzzle.BlockIsMove++;
        _puzzle.CanPlayerMove = false;

        float percent = 0;

        switch (dirToInt(dir))
        {
            case 1:
                if (initialPos.x > target.x)
                {
                    while (percent < 1)
                    {
                        percent += Time.deltaTime / (duration * ((_size * _puzzle.blocksPerLine - initialPos.x) / (_size * _puzzle.blocksPerLine - Math.Abs(initialPos.x - target.x))));
                        transform.position = Vector2.Lerp(initialPos, new Vector2(_size * _puzzle.blocksPerLine, initialPos.y), percent);
                        yield return null;
                    }
                    transform.position = new Vector2(0, initialPos.y);
                    percent = 0;

                    while (percent < 1)
                    {
                        percent += Time.deltaTime / (duration * ((_size * _puzzle.blocksPerLine - Math.Abs(initialPos.x - target.x) - (_size * _puzzle.blocksPerLine - initialPos.x)) / (_size * _puzzle.blocksPerLine - Math.Abs(initialPos.x - target.x))));
                        transform.position = Vector2.Lerp(new Vector2(0, initialPos.y), target, percent);
                        yield return null;
                    }
                }
                else
                {
                    while (percent < 1)
                    {
                        percent += Time.deltaTime / duration;
                        transform.position = Vector2.Lerp(initialPos, target, percent);
                        yield return null;
                    }
                }
                break;
            case 2:
                if (initialPos.y > target.y)
                {
                    while (percent < 1)
                    {
                        percent += Time.deltaTime / (duration * ((_size * _puzzle.blocksPerLine - initialPos.y) / (_size * _puzzle.blocksPerLine - Math.Abs(initialPos.y - target.y))));
                        transform.position = Vector2.Lerp(initialPos, new Vector2(initialPos.x, _size * _puzzle.blocksPerLine), percent);
                        yield return null;
                    }
                    transform.position = new Vector2(0, initialPos.y);
                    percent = 0;
                    while (percent < 1)
                    {
                        percent += Time.deltaTime / (duration * ((_size * _puzzle.blocksPerLine - Math.Abs(initialPos.y - target.y) - (_size * _puzzle.blocksPerLine - initialPos.y)) / (_size * _puzzle.blocksPerLine - Math.Abs(initialPos.y - target.y))));
                        transform.position = Vector2.Lerp(new Vector2(initialPos.x, 0), target, percent);
                        yield return null;
                    }
                }
                else
                {
                    while (percent < 1)
                    {
                        percent += Time.deltaTime / duration;
                        transform.position = Vector2.Lerp(initialPos, target, percent);
                        yield return null;
                    }
                }
                break;
            case 3:
                if (initialPos.x < target.x)
                {
                    while (percent < 1)
                    {
                        percent += Time.deltaTime / (duration * ((initialPos.x / (_size * _puzzle.blocksPerLine - Math.Abs(initialPos.x - target.x)))));
                        transform.position = Vector2.Lerp(initialPos, new Vector2(0, initialPos.y), percent);
                        yield return null;
                    }
                    transform.position = new Vector2(_size * _puzzle.blocksPerLine, initialPos.y);
                    percent = 0;

                    while (percent < 1)
                    {
                        percent += Time.deltaTime / (duration * ((_size * _puzzle.blocksPerLine - target.x)) / (_size * _puzzle.blocksPerLine - Math.Abs(initialPos.x - target.x)));
                        transform.position = Vector2.Lerp(new Vector2(_size * _puzzle.blocksPerLine, initialPos.y), target, percent);
                        yield return null;
                    }
                }
                else
                {
                    while (percent < 1)
                    {
                        percent += Time.deltaTime / duration;
                        transform.position = Vector2.Lerp(initialPos, target, percent);
                        yield return null;
                    }
                }
                break;
            case 4:
                if (initialPos.y < target.y)
                {
                    while (percent < 1)
                    {
                        percent += Time.deltaTime / (duration * ((initialPos.y / (_size * _puzzle.blocksPerLine - Math.Abs(initialPos.y - target.y)))));
                        transform.position = Vector2.Lerp(initialPos, new Vector2(initialPos.x, 0), percent);
                        yield return null;
                    }
                    transform.position = new Vector2(initialPos.x, _size * _puzzle.blocksPerLine);
                    percent = 0;

                    while (percent < 1)
                    {
                        percent += Time.deltaTime / (duration * ((_size * _puzzle.blocksPerLine - target.y)) / (_size * _puzzle.blocksPerLine - Math.Abs(initialPos.y - target.y)));
                        transform.position = Vector2.Lerp(new Vector2(initialPos.x, _size * _puzzle.blocksPerLine), target, percent);
                        yield return null;
                    }
                }
                else
                {
                    while (percent < 1)
                    {
                        percent += Time.deltaTime / duration;
                        transform.position = Vector2.Lerp(initialPos, target, percent);
                        yield return null;
                    }
                }
                break;
            case 0:
                Debug.Log("Block IEAnimateMove dir = 0.");
                break;
        }

        //while (percent < 1)
        //{
        //    percent += Time.deltaTime / duration;
        //    transform.position = Vector2.Lerp(initialPos, target, percent);
        //    yield return null;

        //}
        if (OnFinishMoving != null)
        {
            OnFinishMoving();

        }

    }

    public void MoveBlockToCoorAtMap(int x, int y)
    {
        if (_X - x == 0 || _Y - y == 0)
        {
            if (_X - x < 0 || _Y - y < 0)
            {
                if (_X - x != 0)
                {
                    MoveBlock(x - _X, Vector3.right);
                }
                else
                {
                    MoveBlock(y - _Y, Vector3.up);
                }
            }
            else
            {
                if (_X - x != 0)
                {
                    MoveBlock(_X - x, Vector3.left);
                }
                else
                {
                    MoveBlock(_Y - y, Vector3.down);
                }
            }
        }
    }

    public void MoveBlockToCoorAtMap(int x, int y, int dir)
    {
        Vector3 vec = intToDir(dir);
        if (_X - x == 0 || _Y - y == 0)
        {
            if (_X - x < 0 || _Y - y < 0)
            {
                if (_X - x != 0)
                {
                    MoveBlock(x - _X, vec);
                }
                else
                {
                    MoveBlock(y - _Y, vec);
                }
            }
            else
            {
                if (_X - x != 0)
                {
                    MoveBlock(_X - x, vec);
                }
                else
                {
                    MoveBlock(_Y - y, vec);
                }
            }
        }
    }
    //void GiveCoordOfOtherBlock(int x, int y, List<Block> arr)
    //{
    //    closeCoord_Pos = _MAPtmp[x][y]._startCoord;
    //    closeCoord_X = _MAPtmp[x][y]._X;
    //    closeCoord_Y = _MAPtmp[x][y]._Y;

    //    _startCoord = closeCoord_Pos;
    //    _X = closeCoord_X;
    //    _Y = closeCoord_Y;
    //    //_MAP[_X][_Y] = this;
    //}

    void AddCoordToMove(Vector2 target, Vector2 dir)
    {
        float[] arr = new float[4];
        arr[0] = target.x;
        arr[1] = target.y;
        arr[2] = dir.x;
        arr[3] = dir.y;
        PosToMoveList.Add(arr);
    }

    public Vector2[] coordsToMoveFromBlockData()
    {
        Vector2[] vec = new Vector2[2];
        vec[0] = new Vector2(PosToMoveList[0][0], PosToMoveList[0][1]);
        vec[1] = new Vector2(PosToMoveList[0][2], PosToMoveList[0][3]);
        return vec;
    }

    public void MoveBlock(int Count, int dir)
    {
        MoveBlock(Count, intToDir(dir));
    }


    public void MoveBlock(int Count, Vector3 dir)
    {
        List<Block> BlocksToMove = new List<Block>();
        ArrToMove(dir, BlocksToMove);

        _puzzle.QueueOfQueue.Enqueue(BlocksToMove);

        switch (dirToInt(dir))
        {
            case 1:
                foreach (Block b in BlocksToMove)
                {
                    if (b._X + Count < _puzzle.blocksPerLine)
                    {
                        //b.AnimateMoveToPos(_MAP[b._X + Count][b._Y]._startCoord, _puzzle.durration);
                        b.AddCoordToMove(_MAP[b._X + Count][b._Y]._startCoord, dir);
                        b.closeCoord_Pos = _MAP[b._X + Count][b._Y]._startCoord;
                        b.closeCoord_X = _MAP[b._X + Count][b._Y]._X;
                        b.closeCoord_Y = _MAP[b._X + Count][b._Y]._Y;
                    }
                    else
                    {
                        b.AddCoordToMove(_MAP[b._X + Count - _puzzle.blocksPerLine][b._Y]._startCoord, dir);
                        b.closeCoord_Pos = _MAP[b._X + Count - _puzzle.blocksPerLine][b._Y]._startCoord;
                        b.closeCoord_X = _MAP[b._X + Count - _puzzle.blocksPerLine][b._Y]._X;
                        b.closeCoord_Y = _MAP[b._X + Count - _puzzle.blocksPerLine][b._Y]._Y;
                    }
                }
                foreach (Block b in BlocksToMove)
                {
                    b._startCoord = b.closeCoord_Pos;
                    b._X = b.closeCoord_X;
                    b._Y = b.closeCoord_Y;
                    _MAP[b._X][b._Y] = b;
                }
                break;
            case 2:
                foreach (Block b in BlocksToMove)
                {
                    if (b._Y + Count < _puzzle.blocksPerLine)
                    {
                        b.AddCoordToMove(_MAP[b._X][b._Y + Count]._startCoord, dir);
                        b.closeCoord_Pos = _MAP[b._X][b._Y + Count]._startCoord;
                        b.closeCoord_X = _MAP[b._X][b._Y + Count]._X;
                        b.closeCoord_Y = _MAP[b._X][b._Y + Count]._Y;
                    }
                    else
                    {
                        b.AddCoordToMove(_MAP[b._X][b._Y + Count - _puzzle.blocksPerLine]._startCoord, dir);
                        b.closeCoord_Pos = _MAP[b._X][b._Y + Count - _puzzle.blocksPerLine]._startCoord;
                        b.closeCoord_X = _MAP[b._X][b._Y + Count - _puzzle.blocksPerLine]._X;
                        b.closeCoord_Y = _MAP[b._X][b._Y + Count - _puzzle.blocksPerLine]._Y;
                    }
                }
                foreach (Block b in BlocksToMove)
                {
                    b._startCoord = b.closeCoord_Pos;
                    b._X = b.closeCoord_X;
                    b._Y = b.closeCoord_Y;
                    _MAP[b._X][b._Y] = b;
                }
                break;
            case 3:
                foreach (Block b in BlocksToMove)
                {
                    if (b._X - Count >= 0)
                    {
                        b.AddCoordToMove(_MAP[b._X - Count][b._Y]._startCoord, dir);
                        b.closeCoord_Pos = _MAP[b._X - Count][b._Y]._startCoord;
                        b.closeCoord_X = _MAP[b._X - Count][b._Y]._X;
                        b.closeCoord_Y = _MAP[b._X - Count][b._Y]._Y;
                    }
                    else
                    {
                        b.AddCoordToMove(_MAP[b._X - Count + _puzzle.blocksPerLine][b._Y]._startCoord, dir);
                        b.closeCoord_Pos = _MAP[b._X - Count + _puzzle.blocksPerLine][b._Y]._startCoord;
                        b.closeCoord_X = _MAP[b._X - Count + _puzzle.blocksPerLine][b._Y]._X;
                        b.closeCoord_Y = _MAP[b._X - Count + _puzzle.blocksPerLine][b._Y]._Y;
                    }
                }
                foreach (Block b in BlocksToMove)
                {
                    b._startCoord = b.closeCoord_Pos;
                    b._X = b.closeCoord_X;
                    b._Y = b.closeCoord_Y;
                    _MAP[b._X][b._Y] = b;
                }
                break;
            case 4:
                foreach (Block b in BlocksToMove)
                {
                    if (b._Y - Count >= 0)
                    {
                        b.AddCoordToMove(_MAP[b._X][b._Y - Count]._startCoord, dir);
                        b.closeCoord_Pos = _MAP[b._X][b._Y - Count]._startCoord;
                        b.closeCoord_X = _MAP[b._X][b._Y - Count]._X;
                        b.closeCoord_Y = _MAP[b._X][b._Y - Count]._Y;
                    }
                    else
                    {
                        b.AddCoordToMove(_MAP[b._X][b._Y - Count + _puzzle.blocksPerLine]._startCoord, dir);
                        b.closeCoord_Pos = _MAP[b._X][b._Y - Count + _puzzle.blocksPerLine]._startCoord;
                        b.closeCoord_X = _MAP[b._X][b._Y - Count + _puzzle.blocksPerLine]._X;
                        b.closeCoord_Y = _MAP[b._X][b._Y - Count + _puzzle.blocksPerLine]._Y;
                    }
                }
                foreach (Block b in BlocksToMove)
                {
                    b._startCoord = b.closeCoord_Pos;
                    b._X = b.closeCoord_X;
                    b._Y = b.closeCoord_Y;
                    _MAP[b._X][b._Y] = b;
                }
                break;
            default:
                break;


        }

        _puzzle.CheckNeedOfAnime();
    }    //сдвинуть блок на Н в сторону


    //public void MoveBlockAt(int x, int y) { }       //отправить блок по карте

    public void MoveBlockOnPos(float x, float y)
    {
        if (_puzzle.CanPlayerMove && _puzzle.gameLogic.isItMyTurn && !_puzzle.gameLogic.isMoved)
        {
            point = ScreenPosToWorldPosByPlane(new Vector2(x, y), _planeBlock);
            // Debug.Log("coord: " + _startCoord.x + "point: " + point.x);


            // определить направление
            if ((point.x - _startCoord.x > Math.Abs(point.y - _startCoord.y)) && (point.x - _startCoord.x > _size * 0.45) && (_dirrection == 0))
            {
                _dirrection = 1; //right
            }
            else if (point.y - _startCoord.y > Math.Abs(point.x - _startCoord.x) && Math.Abs(point.y - _startCoord.y) > _size * 0.45 && _dirrection == 0)
            {
                _dirrection = 2; //top
            }
            else if ((-point.x) - _startCoord.x < (-Math.Abs(point.y - _startCoord.y)) && Math.Abs(point.x - _startCoord.x) > _size * 0.45 && _dirrection == 0)
            {
                _dirrection = 3; //left
            }
            else if ((-point.y) - _startCoord.y < (-Math.Abs(point.x - _startCoord.x)) && Math.Abs(point.y - _startCoord.y) > _size * 0.45 && _dirrection == 0)
            {
                _dirrection = 4; //down
            }
            else
            {
                //BlockArrClear();
            }


            // заполнить массив
            if (_dirrection != 0 & !_BlockArrFull)
            {
                switch (_dirrection)
                {
                    case 0:
                        BlockArrClear(BlockArr);
                        break;
                    case 1:
                        ArrToMove(Vector3.right, BlockArr);
                        _BlockArrFull = true;
                        break;
                    case 2:
                        ArrToMove(Vector3.up, BlockArr);
                        _BlockArrFull = true;
                        break;
                    case 3:
                        ArrToMove(Vector3.left, BlockArr);
                        _BlockArrFull = true;
                        break;
                    case 4:
                        ArrToMove(Vector3.down, BlockArr);
                        _BlockArrFull = true;
                        break;
                    default:
                        break;
                }
            }
            else if (_dirrection == 0 & _BlockArrFull)
            {
                BlockArrClear(BlockArr);
            }

            //движение за мышкой
            if (_BlockArrFull)
            {
                foreach (Block b in BlockArr)
                {
                    if (_dirrection == 1 | _dirrection == 3)
                    {
                        b.gameObject.transform.position = new Vector3((b._startCoord.x - _startCoord.x + ((((_size * _puzzle.blocksPerLine * 100) + ((float)0.5 - ((float)_dirrection - 2) / 2) * point.x) + ((float)0.5 + ((float)_dirrection - 2) / 2) * (-Math.Abs(point.x - (_size * (_puzzle.blocksPerLine))))))) % (_size * (_puzzle.blocksPerLine)), b.gameObject.transform.position.y);
                    }
                    else if (_dirrection == 2 | _dirrection == 4)
                    {
                        //(_size *_puzzle.blocksPerLine * 10) - костыль что сдвигает зону на 10 полей вверх
                        //
                        b.gameObject.transform.position = new Vector3(b.gameObject.transform.position.x, (b._startCoord.y - _startCoord.y + ((((_size * _puzzle.blocksPerLine * 100) + ((float)0.5 - ((float)_dirrection - 3) / 2) * point.y) + ((float)0.5 + ((float)_dirrection - 3) / 2) * (-Math.Abs(point.y - (_size * (_puzzle.blocksPerLine))))))) % (_size * (_puzzle.blocksPerLine)));
                    }
                }

                switch (_dirrection)
                {
                    case 0:
                        break;
                    case 1:
                        if (point.x < _startCoord.x) // & dirrection == 1
                            _dirrection = 3;
                        break;
                    case 2:
                        if (point.y < _startCoord.y)
                            _dirrection = 4;
                        break;
                    case 3:
                        if (point.x > _startCoord.x)
                            _dirrection = 1;
                        break;
                    case 4:
                        if (point.y > _startCoord.y)
                            _dirrection = 2;
                        break;
                    default:
                        break;
                }
            }
        }    //сдвинуть блок на координату. По мышке
        else
        {
            if (!_puzzle.CanPlayerMove)
                _puzzle.Warning("You can't move blocks until shufl end.");
            else if (!_puzzle.gameLogic.isItMyTurn)
                _puzzle.Warning("It isn't your turn.");
            else if(_puzzle.gameLogic.isMoved)
                _puzzle.Warning("You already moved this turn.");
        }
    }
    public void BlockArrClear(List<Block> Arr)
    {
        foreach (Block b in Arr)
        {
            b.transform.position = b.closeCoord_Pos;
        }

        //BlockArr.RemoveRange(0, _puzzle.blocksPerLine);
        Debug.Log("BlockArrClear");
        Arr.Clear();
        _dirrection = 0;
        _BlockArrFull = false;
    }

    public void OnMouseUp()
    {
        mapSync();
    }

    void mapSync()
    {
        if (_BlockArrFull)
        {
            foreach (Block b in BlockArr)
            {
                foreach (Block d in BlockArr)
                {
                    if (Math.Sqrt(Math.Pow((b.gameObject.transform.position.x - d._startCoord.x), 2) + Math.Pow((d._startCoord.y - b.gameObject.transform.position.y), 2)) < Math.Sqrt(Math.Pow((b.gameObject.transform.position.x - b.closeCoord_Pos.x), 2) + Math.Pow((b.gameObject.transform.position.y - b.closeCoord_Pos.y), 2)))
                    {
                        b._lastX = b._X;
                        b._lastY = b._Y;

                        b.closeCoord_Pos = d._startCoord;
                        b.closeCoord_X = d._X;
                        b.closeCoord_Y = d._Y;
                    }
                }
            }

            _puzzle.gameLogic.MoveBlock(closeCoord_X, closeCoord_Y, _dirrection, _lastX, _lastY);
            //int raznitsaX = -(_X - closeCoord_X);
            //int raznitsaY = -(_Y - closeCoord_Y);

            //if (raznitsaY != 0)
            //{
            //    if (raznitsaY > 0)
            //    {
            //        foreach (Block b in BlockArr)
            //        {
            //            if (b.closeCoord_Y - raznitsaY >= 0)
            //            {
            //                MAP[b.closeCoord_X][b.closeCoord_Y] = b;
            //                b._X = b.closeCoord_X;
            //                b._Y = b.closeCoord_Y - raznitsaY;
            //            }
            //            else
            //            {
            //                MAP[b.closeCoord_X][b.closeCoord_Y - raznitsaY + _puzzle.blocksPerLine] = b;
            //                b._X = b.closeCoord_X;
            //                b._Y = b.closeCoord_Y;
            //            }
            //        }
            //    }
            //    else
            //    {
            //        foreach (Block b in BlockArr)
            //        {
            //            if (b.closeCoord_Y + raznitsaY < _puzzle.blocksPerLine)
            //            {
            //                MAP[b.closeCoord_X][b.closeCoord_Y] = b;
            //                b._X = b.closeCoord_X;
            //                b._Y = b.closeCoord_Y - raznitsaY;
            //            }
            //            else
            //            {
            //                MAP[b.closeCoord_X][b.closeCoord_Y] = b;
            //                b._X = b.closeCoord_X;
            //                b._Y = b.closeCoord_Y - raznitsaY - _puzzle.blocksPerLine;
            //            }
            //        }
            //    }
            //}
            //else
            //{
            //    if (raznitsaX > 0)
            //    {
            //        foreach (Block b in BlockArr)
            //        {
            //            if (b.closeCoord_X - raznitsaX >= 0)
            //            {
            //                MAP[b.closeCoord_X][b.closeCoord_Y] = b;
            //                b._X = b.closeCoord_X - raznitsaX;
            //                b._Y = b.closeCoord_Y;
            //            }
            //            else if (b.closeCoord_X - raznitsaX < 0)
            //            {
            //                MAP[b.closeCoord_X][b.closeCoord_Y] = b;
            //                b._X = b.closeCoord_X - raznitsaX + _puzzle.blocksPerLine;
            //                b._Y = b.closeCoord_Y;
            //            }
            //        }
            //    }
            //    else
            //    {
            //        foreach (Block b in BlockArr)
            //        {
            //            if (b.closeCoord_X - raznitsaX <= _puzzle.blocksPerLine)
            //            {
            //                MAP[b.closeCoord_X][b.closeCoord_Y] = b;
            //                b._X = b.closeCoord_X - raznitsaX;
            //                b._Y = b.closeCoord_Y;
            //            }
            //            else
            //            {
            //                MAP[b.closeCoord_X][b.closeCoord_Y] = b;
            //                b._X = b.closeCoord_X - raznitsaX - _puzzle.blocksPerLine;
            //                b._Y = b.closeCoord_Y;
            //            }
            //        }
            //    }
            //}

            //List<List<Block>> TMP = CopyListList(MAP);
            //Block block = new Block();

            foreach (Block b in BlockArr)
            {
                b._startCoord = b.closeCoord_Pos;
                b._X = b.closeCoord_X;
                b._Y = b.closeCoord_Y;
                _MAP[b._X][b._Y] = b;




                //MAP[b.closeCoord_X][b.closeCoord_Y]._startCoord = b.closeCoord_Pos;
                //MAP[b.closeCoord_X][b.closeCoord_Y].gameObject.transform.position = b.closeCoord_Pos;
                //MAP[b.closeCoord_X][b.closeCoord_Y] = MAP[b._X][b._Y];

                //MAP[b._X][b._Y]._X = b._X;
                //MAP[b._X][b._Y]._Y = b._Y;
            }

            //foreach (Block b in BlockArr)
            //{
            //    TMP[b.closeCoord_X][b.closeCoord_Y].closeCoord = b;
            //}

            //MAP = new List<List<Block>>(TMP);

            //for (int x = 0; x < _puzzle.blocksPerLine; x++)
            //{
            //    for (int y = 0; y < _puzzle.blocksPerLine; y++)
            //    {
            //        MAP[x][y] = TMP[x][y];
            //        MAP[x][y].gameObject.transform.position = MAP[x][y]._startCoord;
            //    }
            //}

            //for (int x = 0; x < _puzzle.blocksPerLine; x++)
            //{
            //    for (int y = 0; y < _puzzle.blocksPerLine; y++)
            //    {
            //        MAP[x][y].transform.position = MAP[x][y].closeCoord._startCoord;
            //    }
            //}

            BlockArrClear(BlockArr);
        }
    }

    void ArrToMove(Vector3 dir, List<Block> Arr)
    {
        if ((Vector3.zero + dir).y == 0)
        {
            for (int i = 0; i < _puzzle.blocksPerLine; i++)
            {
                if (_X + i < _puzzle.blocksPerLine)
                {
                    Arr.Add(_MAP[_X + i][_Y]);
                }
                if (_X - i >= 0 && i != 0)
                {
                    Arr.Add(_MAP[_X - i][_Y]);
                }
            }
        }
        else
        {
            for (int i = 0; i < _puzzle.blocksPerLine; i++)
            {
                if (_Y + i < _puzzle.blocksPerLine)
                {
                    Arr.Add(_MAP[_X][_Y + i]);
                }
                if (_Y - i >= 0 && i != 0)
                {
                    Arr.Add(_MAP[_X][_Y - i]);
                }
            }
        }
        _BlockArrFull = true;
    }

    int dirToInt(Vector3 dir)
    {
        if ((Vector3.zero + dir).x > 0)
        {
            return 1;
        }
        else if ((Vector3.zero + dir).y > 0)
        {
            return 2;
        }
        else if ((Vector3.zero + dir).x < 0)
        {
            return 3;
        }
        else if ((Vector3.zero + dir).y < 0)
        {
            return 4;
        }
        else
        {
            return 0;
        }
    }

    Vector3 intToDir(int d)
    {
        Vector3 vec = new Vector3();
        switch (d)
        {
            case 1:
                vec = Vector3.right;
                break;
            case 2:
                vec = Vector3.up;
                break;
            case 3:
                vec = Vector3.left;
                break;
            case 4:
                vec = Vector3.down;
                break;
            case 0:
                vec = Vector3.zero;
                break;
        }
        return vec;
    }
}
