﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticDescrBoxInfo : MonoBehaviour
{
    public string name, descr;
    

    private void Awake()
    {
        DescrBoxScr boxScr = new DescrBoxScr(); 
        boxScr = gameObject.AddComponent<DescrBoxScr>();
        boxScr.SetDescrObj(new StaticInfoBox(name, descr));
    }
}
