﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DescrBoxScr : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public IDescrBox descrObj;

    public GameObject descrBoxTemplate;

    private float t = 3;
    

    private GameObject descrBox;
    private bool boxIsCreated = false;

    public DescrBoxScr(IDescrBox descrObj)
    {
        this.descrObj = descrObj;
    }


    public DescrBoxScr(StaticInfoBox box)
    {
        descrObj = box;
    }

    public DescrBoxScr()
    {
    }

    public IDescrBox GetDescrObj()
    {
        return descrObj;
    }

    public void SetDescrObj(IDescrBox box)
    {
        descrObj = box;
    }

    //void OnMouseOver()
    //{
    //    Debug.Log("MouseOver, time: " + t);
    //    t -= Time.deltaTime;
        
    //    if (t <= 0 && !boxIsCreated)
    //    {
    //        CreateDescrBox();
    //    }
    //}

    void CreateDescrBox()
    {
        descrBox = Instantiate(descrBoxTemplate, Input.mousePosition, Quaternion.identity);
        descrBox.transform.parent = gameObject.transform;
        UpdateBoxInfo();

    }

    private void Update()
    {
        if (boxIsCreated)
        {
            UpdateBoxInfo();
        }
    }



    void UpdateBoxInfo()
    {
        Text Dtext = descrBox.transform.Find("DescrBoxTextPanel").Find("DescrBoxText").GetComponent<Text>();
        Text Ntext = descrBox.transform.Find("NameDecrText").GetComponent<Text>();
        Ntext.text = descrObj.GetName();
        Dtext.text = descrObj.GetDescription();
    }

    void DestroyDescrBox()
    {
        Destroy(descrBox);
    }

    //void OnMouseExit()
    //{
    //    t = 3;
    //}

    public void OnPointerEnter(PointerEventData eventData)
    {
        CreateDescrBox();

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        DestroyDescrBox();
    }
}

public class StaticInfoBox : IDescrBox
{
    public string name, desk;

    public StaticInfoBox(string name, string desk)
    {
        this.name = name;
        this.desk = desk;
    }

    public string GetDescription()
    {
        return desk;
    }

    public string GetName()
    {
        return name;
    }
}



public interface IDescrBox
{
    string GetName();
    string GetDescription();
}