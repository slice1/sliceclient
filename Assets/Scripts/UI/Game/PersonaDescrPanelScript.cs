﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PersonaDescrPanelScript : MonoBehaviour
{
    public Character _char;
    public Text _charPow, _charWis, _charLoyal, _charName, _charItemName, _charItemDescr;
    public Image _charPortrait;



    public void syncData()
    {
        _charPow.text = System.Convert.ToString(_char._power);
        _charWis.text = System.Convert.ToString(_char._wisdom);
        _charLoyal.text = System.Convert.ToString(_char._loyalty);

        _charName.text = _char._firstName;
        _charPortrait.sprite = _char._sprite;
    }


}
