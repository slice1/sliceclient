﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestrScript : MonoBehaviour
{
    public GameObject gameObject;

    public void Destroy()
    {
        Destroy(gameObject);
    }
}
