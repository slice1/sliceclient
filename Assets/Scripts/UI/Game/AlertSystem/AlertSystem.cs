﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlertSystem 
{
    VisualSystem visualSystem;

    public AlertSystem(VisualSystem visualSystem)
    {
        this.visualSystem = visualSystem;
    }


    public void Warn(string str)
    {
        Warn(str, 5);
    }

    public void Warn(string str, float dur)
    {
        Warn(str, dur, 1);
    }

    public void Warn(string str, float dur, float speed)
    {
        visualSystem.Warn(str, dur, speed);
    }

    public void MessageBox(string str)
    {
        visualSystem.MessageBox(str);
    }
}
