﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VisualSystem : MonoBehaviour
{
    public event System.Action OnFinishAplha;

    public Queue<ContOfWarn> QueueOfWarnMessage = new Queue<ContOfWarn>();

    public GameObject UIPanel;

    public GameObject MessageBoxPrefab;

    public Text WarnText;

    private bool AlphaTextChange = false;

    

    
    void Start()
    {
        OnFinishAplha += CheckNeedOfWarn;
    }
    

    public void MessageBox(string str)
    {
        GameObject obj = Instantiate(MessageBoxPrefab, UIPanel.transform);
        Text text = obj.transform.Find("TextPanel").Find("MessageBoxText").GetComponent<Text>();
        text.text = str;
    }

    public void Warn(string str, float durr)
    {
        Warn(str, durr, 1);
    }

    public void Warn(string str, float durr, float speed)
    {
        AddWarnMessage(str, durr, speed);
        CheckNeedOfWarn();
    }

    IEnumerator WarnAlphaChange(string str, float duration, float speed)
    {
        WarnText.text = str;

        AlphaTextChange = true;

        float percent = 0;



        while (percent < 1)
        {
            percent += Time.deltaTime / speed;
            Color col = new Color(255, 170, 20, Vector2.Lerp(Vector2.zero, Vector2.right, percent).x);
            WarnText.color = col;
            yield return null;

        }
        percent = 0;
        while (percent < 1)
        {
            percent += Time.deltaTime / duration;
            yield return null;

        }
        percent = 0;
        while (percent < 1)
        {
            percent += Time.deltaTime / speed;
            Color col = new Color(255, 170, 20, Vector2.Lerp(Vector2.right, Vector2.zero, percent).x);
            WarnText.color = col;
            yield return null;
        }

        if (OnFinishAplha != null)
        {
            OnFinishAplha();
        }
    }

    private void AddWarnMessage(string str, float durr, float speed)
    {
        QueueOfWarnMessage.Enqueue(new ContOfWarn(str, durr, speed));
    }

    void CheckNeedOfWarn()
    {
        if (QueueOfWarnMessage.Count > 0)
        {
            ContOfWarn content = QueueOfWarnMessage.Dequeue();
            StartCoroutine(WarnAlphaChange(content.contText, content.speed, content.durr));
        }
    }
}

public class ContOfWarn
{
    public string contText;
    public float speed, durr;

    public ContOfWarn(string contText, float durr, float speed)
    {
        this.contText = contText;
        this.speed = speed;
        this.durr = durr;
    }
}