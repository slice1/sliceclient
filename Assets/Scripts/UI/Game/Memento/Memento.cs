﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Memento 
{

    public string name;
    public int pow, wis, loy;

    public Sprite sprite;

    public Memento(Character memChar)
    {
        this.name = memChar._firstName;
        this.pow = memChar._power;
        this.wis = memChar._wisdom;
        this.loy = memChar._loyalty;
        sprite = memChar._sprite;
    }
}

public class GameBuff
{
    public Stack<Memento> History { get; private set; }
    public GameBuff()
    {
        History = new Stack<Memento>();
    }
}
