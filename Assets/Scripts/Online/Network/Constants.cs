﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants : MonoBehaviour {

    public static Constants instance;

    private void Awake()
    {
        instance = this;
    }

    public const int MAX_PLAYERS = 100;
}
