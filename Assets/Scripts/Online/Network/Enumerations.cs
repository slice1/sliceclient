﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enumerations : MonoBehaviour {
    public static Enumerations instance;

    private void Awake()
    {
        instance = this;
    }

    public enum ServerPackets
    {
        SAlertMsg = 1,
        SPlayerData,
        SLobbyUpdate,
        SLobbyCreated,
        SLobbyFound,
        SLobbyAddPlayer,
        SLobbyStart,
        SGameStartTurn,
        SGameMoveBlock,
        SGameEndTurn
    }

    public enum ClientPackets
    {
        CNewAccount = 1,
        CLoginAndCreateLobby,
        CLoginAndSearchLobby,
        CStartGame,
        CMoveBlock,
        CDoAct,
        CEndTurn
    }
}
