﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Globals : MonoBehaviour
{
    public static Globals instance;

    // Use this for initialization
    void Awake()
    {
        instance = this;
    }

    public int MyIndex;
    public int EnemyIndex;
}