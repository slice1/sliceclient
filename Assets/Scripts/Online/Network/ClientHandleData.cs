﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClientHandleData : MonoBehaviour
{

    private delegate void Packet_(byte[] Data);
    private Dictionary<int, Packet_> Packets;
    

    [Header("PlayerPref")]
    public Lobby lobby;
    public GameObject LogInWindow;
    public GameObject LobbyWindow;
    

    public void InitMessages()
    {
        Packets = new Dictionary<int, Packet_>();
        Packets.Add((int)Enumerations.ServerPackets.SAlertMsg, HandleAlertMsg);
        //Packets.Add((int)Enumerations.ServerPackets.SLogin, HandleLogin);
        Packets.Add((int)Enumerations.ServerPackets.SLobbyUpdate, HandleLobbyUpdate);
        Packets.Add((int)Enumerations.ServerPackets.SLobbyCreated, HandleLobbyCreated);
        Packets.Add((int)Enumerations.ServerPackets.SLobbyFound, HandleLobbyFound);
        Packets.Add((int)Enumerations.ServerPackets.SLobbyAddPlayer, HandleLobbyAddPlayer);
        Packets.Add((int)Enumerations.ServerPackets.SLobbyStart, HandleLobbyStart);
        Packets.Add((int)Enumerations.ServerPackets.SGameEndTurn, HandleEndTurn);
        Packets.Add((int)Enumerations.ServerPackets.SGameMoveBlock, HandleMoveBlock);
        Packets.Add((int)Enumerations.ServerPackets.SGameStartTurn, HandleStartTurn);


        //Packets.Add((int)Enumerations.ServerPackets.SDeckChanges, HandleDeckChanges);
        //Packets.Add((int)Enumerations.ServerPackets.SPlayerData, HandlePlayerData);

    }

    public static ClientHandleData instance;

    private void Awake()
    {
        instance = this;
        InitMessages();

    }

  

    public void HandleData(byte[] data)
    {

        int packetnum;
        Packet_ Packet;
        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteBytes(data);
        packetnum = buffer.ReadInteger();
        buffer = null;
        if (packetnum == 0)
            return;

        if (Packets.TryGetValue(packetnum, out Packet))
        {
            Packet.Invoke(data);
        }
    }

    void HandleAlertMsg(byte[] data)
    {
        int packetnum;
        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteBytes(data);
        packetnum = buffer.ReadInteger();

        string AlertMsg = buffer.ReadString();

        Debug.Log(AlertMsg);
    }

    //void HandleLogin(byte[] data)
    //{
    //    int packetnum;
    //    ByteBuffer buffer = new ByteBuffer();
    //    buffer.WriteBytes(data);
    //    packetnum = buffer.ReadInteger();
    //    string name = buffer.ReadString();

    //    lobby.ownPlayer = lobby.gameObject.AddComponent<Player>();

    //    lobby.ownPlayer._name = name;

    //    LogInWindow.SetActive(false);

    //}





    ///GAME LOGIC//////GAME LOGIC//////GAME LOGIC//////GAME LOGIC//////GAME LOGIC//////GAME LOGIC//////GAME LOGIC//////GAME LOGIC//////GAME LOGIC//////GAME LOGIC//////GAME LOGIC//////GAME LOGIC//////GAME LOGIC//////GAME LOGIC///


    void HandleEndTurn(byte[] data)
    {
        int packetnum;
        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteBytes(data);
        packetnum = buffer.ReadInteger();

        lobby.game.gameLogic.EndTurn();
    }

    void HandleMoveBlock(byte[] data)
    {
        int packetnum;

        int _X;
        int _Y;
        int Dir;
        int Distance;

        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteBytes(data);
        packetnum = buffer.ReadInteger();
        _X = buffer.ReadInteger();
        _Y = buffer.ReadInteger();
        Dir = buffer.ReadInteger();
        Distance = buffer.ReadInteger();

        lobby.game.BlockMap[_X][_Y].MoveBlock(Distance, Dir);
    }

    void HandleStartTurn(byte[] data)
    {
        int packetnum;
        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteBytes(data);
        packetnum = buffer.ReadInteger();

        lobby.game.gameLogic.StartTurn();
    }


    ///ENDGAMELOGIC//////ENDGAMELOGIC//////ENDGAMELOGIC//////ENDGAMELOGIC//////ENDGAMELOGIC//////ENDGAMELOGIC//////ENDGAMELOGIC//////ENDGAMELOGIC//////ENDGAMELOGIC//////ENDGAMELOGIC//////ENDGAMELOGIC//////ENDGAMELOGIC//////ENDGAMELOGIC///







    ///LOBBY//////LOBBY//////LOBBY//////LOBBY//////LOBBY//////LOBBY//////LOBBY//////LOBBY//////LOBBY//////LOBBY//////LOBBY//////LOBBY//////LOBBY//////LOBBY//////LOBBY//////LOBBY//////LOBBY///

    void HandleLobbyUpdate(byte[] data)
    {
        int packetnum;
        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteBytes(data);
        packetnum = buffer.ReadInteger();


    }

    void HandleLobbyCreated(byte[] data) 
    {
        int packetnum, lobbyInd, ownInd;
        string name;
        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteBytes(data);
        packetnum = buffer.ReadInteger();
        lobbyInd = buffer.ReadInteger();
        ownInd = buffer.ReadInteger();
        name = buffer.ReadString();

        lobby._ServerIndex = lobbyInd;
        lobby.AddPlayer(name, ownInd);

        lobby.SetOwnPlayer(name);

        LogInWindow.SetActive(false);
    }

    void HandleLobbyFound(byte[] data) 
    {
        int packetnum, lobbyInd, countOfPlayers;

        int tmpInt;
        string tmpStr, owname;

        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteBytes(data);
        packetnum = buffer.ReadInteger();
        owname = buffer.ReadString();
        lobbyInd = buffer.ReadInteger();
        countOfPlayers = buffer.ReadInteger();

        for (int i = 0; i < countOfPlayers; i++)
        {
            tmpInt = buffer.ReadInteger();
            tmpStr = buffer.ReadString();
            lobby.AddPlayer(tmpStr, tmpInt);
        }

        lobby.SetOwnPlayer(owname);
        LogInWindow.SetActive(false);

    }

    void HandleLobbyAddPlayer(byte[] data)
    {
        int packetnum, playerInd;
        string playerName;
        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteBytes(data);
        packetnum = buffer.ReadInteger();
        playerInd = buffer.ReadInteger();
        playerName = buffer.ReadString();

        lobby.AddPlayer(playerName, playerInd);
    }

    void HandleLobbyStart(byte[] data) 
    {
        int packetnum, count, diff, tmpint;
        string tmpstr;

        List<List<Character>> characters = new List<List<Character>>();

        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteBytes(data);
        packetnum = buffer.ReadInteger();
        count = buffer.ReadInteger();
        

        for (int i = 0; i < count; i++)
        {
            tmpstr = buffer.ReadString();
            tmpint = buffer.ReadInteger();

            lobby.SetFactionByName(tmpstr, tmpint);
        }
        

        diff = buffer.ReadInteger();


        List<List<int[]>> map = new List<List<int[]>>();

        //int i0 ,i1,i2 ,i3,i4, i5,i6,i7 , i8 ,i9 , i10 , i11 , i12;

        for (int x = 0; x < diff; x++)
        {
            map.Add(new List<int[]>());
            for (int y = 0; y < diff; y++)
            {
                map[x].Add(new int[13]);
            }
        }
        

        for (int y = 0; y < diff; y++)
        {
            for (int x = 0; x < diff; x++)
            {
                for (int i = 0; i < 13; i++)
                {
                    map[y][x][i] = buffer.ReadInteger();
                }
            }
        }


        for (int y = 0; y < diff; y++)
        {

            characters.Add(new List<Character>());
            for (int x = 0; x < diff; x++)
            {


                characters[y].Add(lobby.game.charGen.CreateCharacter(map[y][x][0], map[y][x][1], map[y][x][2], map[y][x][3], map[y][x][4], map[y][x][5], map[y][x][6], map[y][x][7], map[y][x][8], map[y][x][9], map[y][x][10]));
            }
        }

        //for (int y = 0; y < diff; y++)
        //    {
        //        characters.Add(new List<Character>());
        //        for (int x = 0; x < diff; x++)
        //        {
        //        //    DEBUGTEXT.text = DEBUGTEXT.text + System.Convert.ToString(x);
        //        //    DEBUGTEXT.text = DEBUGTEXT.text + "8";

        //        DEBUGTEXT.text = DEBUGTEXT.text + "S";

        //        characters[y].Add(lobby.game.charGen.CreateCharacter());
        //            DEBUGTEXT.text = DEBUGTEXT.text + "9";
        //        }
        //    }
        

        lobby.game.CreatePuzzle(characters);
        LobbyWindow.SetActive(false);
    }

    ///ENDLOBBY//////ENDLOBBY//////ENDLOBBY//////ENDLOBBY//////ENDLOBBY//////ENDLOBBY//////ENDLOBBY//////ENDLOBBY//////ENDLOBBY//////ENDLOBBY//////ENDLOBBY//////ENDLOBBY//////ENDLOBBY//////ENDLOBBY///


    //void HandleDeckChanges(byte[] data)
    //{
    //    int packetnum;
    //    ByteBuffer buffer = new ByteBuffer();
    //    buffer.WriteBytes(data);
    //    packetnum = buffer.ReadInteger();


    //    int index = buffer.ReadInteger();
    //    //string playDeckDeck = buffer.ToString();
    //    Deck PlayerDeck = buffer.GetDeck(buffer.ReadBytes(4));
    //    Deck GDeckDeck = buffer.GetDeck(buffer.ReadBytes(4));
    //    Deck GDeckHand = buffer.GetDeck(buffer.ReadBytes(4));
    //    Deck GDeckBF = buffer.GetDeck(buffer.ReadBytes(4));
    //    Deck GDeckGrave = buffer.GetDeck(buffer.ReadBytes(4));
    //    Deck GDeckStack = buffer.GetDeck(buffer.ReadBytes(4));
    //    Deck GEnemyDeckStack = buffer.GetDeck(buffer.ReadBytes(4));
    //    Deck GEnemyDeckBattlefield = buffer.GetDeck(buffer.ReadBytes(4));
    //    int count = buffer.ReadInteger();

    //    GameObject _netPlayer;
    //    if (_netPlayer = GameObject.Find("Player: " + index))
    //    {
    //        _netPlayer.GetComponent<NetPlayer>();
    //    }
    //}

    //void HandlePlayerData(byte[] data)
    //{
    //    int packetnum;
    //    ByteBuffer buffer = new ByteBuffer();
    //    buffer.WriteBytes(data);
    //    packetnum = buffer.ReadInteger();
    //    int index = buffer.ReadInteger();
    //    if (Globals.instance.MyIndex < 1)
    //    {
    //        Globals.instance.MyIndex = index;
    //    }

    //    LoginWindow.SetActive(false);

    //    Account.instance[index]._index = index;
    //    General.instance.SetPlayerDeck(index, buffer.GetDeck(buffer.ReadBytes(4)));
    //    General.instance.SetPlayerGDeckDeck(index, buffer.GetDeck(buffer.ReadBytes(4)));
    //    General.instance.SetPlayerGDeckHand(index, buffer.GetDeck(buffer.ReadBytes(4)));
    //    General.instance.SetPlayerGDeckBF(index, buffer.GetDeck(buffer.ReadBytes(4)));
    //    General.instance.SetPlayerGDeckGrave(index, buffer.GetDeck(buffer.ReadBytes(4)));
    //    General.instance.SetPlayerGDeckStack(index, buffer.GetDeck(buffer.ReadBytes(4)));
    //    General.instance.SetEnemyGDeckStack(index, buffer.GetDeck(buffer.ReadBytes(4)));
    //    General.instance.SetEnemyGDeckBF(index, buffer.GetDeck(buffer.ReadBytes(4)));
    //    General.instance.GetEnemyHandCount(index);
    //    General.instance.SetPlayerTime(index,buffer.ReadFloat());
    //    General.instance.SetPlayerHP(index, buffer.ReadInteger());
    //    General.instance.SetPlayerInf(index, buffer.ReadInteger());

    //    Account.instance[index]._name = buffer.ReadString();

    //    PlayerPref.GetComponent<NetPlayer>().Index = index;
    //    PlayerPref.name = "Player: " + index;

    //}
}

//(int index, Deck deck, Deck GDeckDeck, Deck GDeckHand, Deck GDeckBF, Deck GDeckGrave, Deck GDeckStack, Deck GEnemyDeckStack, Deck GEnemyDeckBattlefield, int count, float time, int hp, int inf)