﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ClientSendData : MonoBehaviour
{
    public static ClientSendData instance;
    public Network network;



    [Header("Login")]
    public Text _loginUser;
    public Text _loginPass;
    public Text _loginLobbyName;

    [Header("StartGame")]
    public Text _DifText;

    // Use this for initialization
    void Awake()
    {
        instance = this;
    }

    public void SendDataToServer(byte[] data)
    {
        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteBytes(data);
        Network.instance.myStream.Write(buffer.ToArray(), 0, buffer.ToArray().Length);
        buffer = null;
    }

    public void SendNewAccount()
    {
        ByteBuffer buffer = new ByteBuffer();

        buffer.WriteInteger((int)Enumerations.ClientPackets.CNewAccount);
        buffer.WriteString(_loginUser.text);
        buffer.WriteString(_loginPass.text);

        SendDataToServer(buffer.ToArray());
        buffer = null;
    }

    public void SendLoginAndCreateLobby()
    {
        ByteBuffer buffer = new ByteBuffer();

        buffer.WriteInteger((int)Enumerations.ClientPackets.CLoginAndCreateLobby);
        buffer.WriteString(_loginUser.text);
        buffer.WriteString(_loginPass.text);
        buffer.WriteString(_loginLobbyName.text);

        SendDataToServer(buffer.ToArray());
        buffer = null;
    }

    public void SendLoginSearchForLobby()
    {
        ByteBuffer buffer = new ByteBuffer();

        buffer.WriteInteger((int)Enumerations.ClientPackets.CLoginAndSearchLobby);
        buffer.WriteString(_loginUser.text);
        buffer.WriteString(_loginPass.text);
        buffer.WriteString(_loginLobbyName.text);
        SendDataToServer(buffer.ToArray());
        buffer = null;
    }

    public void SendStartLobby()
    {
        ByteBuffer buffer = new ByteBuffer();

        buffer.WriteInteger((int)Enumerations.ClientPackets.CStartGame);

        if (_DifText.text == "" || _DifText == null)
            buffer.WriteInteger(7);
        else
            buffer.WriteInteger(System.Convert.ToInt32(_DifText.text));
        

        SendDataToServer(buffer.ToArray());
        buffer = null;
    }


    public void SendMoveBlock(int _X, int _Y, int Dirrection, int Distan)
    {
        ByteBuffer buffer = new ByteBuffer();

        buffer.WriteInteger((int)Enumerations.ClientPackets.CMoveBlock);
        buffer.WriteInteger(_X);
        buffer.WriteInteger(_Y);
        buffer.WriteInteger(Dirrection);
        buffer.WriteInteger(Distan);
        SendDataToServer(buffer.ToArray());
        buffer = null;
    }

    //public void SendDoAct()
    //{
    //    ByteBuffer buffer = new ByteBuffer();

    //    buffer.WriteInteger((int)Enumerations.ClientPackets.CDoAct);
    //    buffer.WriteString(_loginUser.text);
    //    buffer.WriteString(_loginPass.text);
    //    buffer.WriteString(_loginLobbyName.text);
    //    SendDataToServer(buffer.ToArray());
    //    buffer = null;
    //}

    public void SendEndTurn()
    {
        ByteBuffer buffer = new ByteBuffer();

        buffer.WriteInteger((int)Enumerations.ClientPackets.CEndTurn);

        SendDataToServer(buffer.ToArray());
        buffer = null;
    }
}
